# REST supplier consumer example #

### Project description ###
The project demonstrates:

* REST supplier (remote server imitation)
* REST consumer
* Interaction between the supplier and the consumer
* Using JDBC

The project uses:

* Spring Boot
* Spring (MVC)
* JDBC
* Thymeleaf
* AngularJS