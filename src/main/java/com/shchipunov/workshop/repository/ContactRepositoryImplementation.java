package com.shchipunov.workshop.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.shchipunov.workshop.domain.Contact;

/**
 * Presents an implementation of {@link ContactRepository}
 * 
 * @author Shchipunov Stanislav
 */
@Repository
@Transactional
public class ContactRepositoryImplementation implements ContactRepository {

	private static final String FIND_BY_ID_STATEMENT = "SELECT * FROM contact WHERE id = ?";
	private static final String FIND_BY_VALUE_STATEMENT = "SELECT * FROM contact WHERE value = ?";
	private static final String FIND_ALL_STATEMENT = "SELECT * FROM contact";
	private static final String SAVE_STATEMENT = "INSERT INTO contact(type, value) VALUES(?, ?)";
	private static final String DELETE_STATEMENT = "DELETE FROM contact WHERE id = ?";

	private final DataSource dataSource;

	@Autowired
	public ContactRepositoryImplementation(DataSource dataSource) {
		this.dataSource = Objects.requireNonNull(dataSource);
	}

	/**
	 * Finds a contact by identification number
	 */
	@Override
	@Transactional(readOnly = true)
	public Contact findOne(Long id) {
		try (Connection connection = dataSource.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(FIND_BY_ID_STATEMENT);) {
			preparedStatement.setLong(1, id);
			try (ResultSet resultSet = preparedStatement.executeQuery()) {
				if (resultSet.next()) {
					return mapContact(resultSet);
				}
				return null;
			}
		} catch (SQLException exception) {
			exception.printStackTrace();
			throw new RuntimeException(exception);
		}
	}

	/**
	 * Finds a contact by its value
	 */
	@Override
	@Transactional(readOnly = true)
	public Contact findByValue(String value) {
		try (Connection connection = dataSource.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(FIND_BY_VALUE_STATEMENT);) {
			preparedStatement.setString(1, value);
			try (ResultSet resultSet = preparedStatement.executeQuery()) {
				if (resultSet.next()) {
					return mapContact(resultSet);
				}
				return null;
			}
		} catch (SQLException exception) {
			exception.printStackTrace();
			throw new RuntimeException(exception);
		}
	}

	/**
	 * Finds all contacts
	 */
	@Override
	@Transactional(readOnly = true)
	public List<Contact> findAll() {
		try (Connection connection = dataSource.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(FIND_ALL_STATEMENT);) {
			try (ResultSet resultSet = preparedStatement.executeQuery()) {
				List<Contact> contacts = new ArrayList<>();
				while (resultSet.next()) {
					contacts.add(mapContact(resultSet));
				}
				return contacts;
			}
		} catch (SQLException exception) {
			exception.printStackTrace();
			throw new RuntimeException(exception);
		}
	}

	/**
	 * Saves a contact
	 */
	@Override
	public Contact save(Contact contact) {
		try (Connection connection = dataSource.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(SAVE_STATEMENT,
						Statement.RETURN_GENERATED_KEYS);) {
			preparedStatement.setString(1, contact.getType());
			preparedStatement.setString(2, contact.getValue());
			preparedStatement.executeUpdate();
			try (ResultSet resultSet = preparedStatement.getGeneratedKeys()) {
				if (resultSet.next()) {
					contact.setId(resultSet.getLong(1));
				}
				return contact;
			}
		} catch (SQLException exception) {
			exception.printStackTrace();
			throw new RuntimeException(exception);
		}
	}

	/**
	 * Deletes a contact by identification number
	 */
	@Override
	public void delete(Long id) {
		try (Connection connection = dataSource.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(DELETE_STATEMENT);) {
			preparedStatement.setLong(1, id);
			preparedStatement.executeUpdate();
		} catch (SQLException exception) {
			exception.printStackTrace();
		}
	}

	private Contact mapContact(ResultSet resultSet) throws SQLException {
		Contact contact = new Contact();
		contact.setId(resultSet.getLong("id"));
		contact.setType(resultSet.getString("type"));
		contact.setValue(resultSet.getString("value"));
		return contact;
	}
}
