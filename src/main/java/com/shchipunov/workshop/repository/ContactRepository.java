package com.shchipunov.workshop.repository;

import java.util.List;

import com.shchipunov.workshop.domain.Contact;

/**
 * Repository to access {@link Contact} instances
 * 
 * @author Shchipunov Stanislav
 */
public interface ContactRepository {

	Contact findOne(Long id);

	Contact findByValue(String value);

	List<Contact> findAll();

	Contact save(Contact user);

	void delete(Long id);
}
