package com.shchipunov.workshop.exception;

/**
 * @author Shchipunov Stanislav
 */
public class ContactNotModifiedException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public ContactNotModifiedException() {
		super();
	}
}
