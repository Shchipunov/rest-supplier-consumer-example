package com.shchipunov.workshop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

/**
 * @author Shchipunov Stanislav
 */
@SpringBootApplication
public class Launcher {

	public static void main(String[] arguments) {
		SpringApplication.run(Launcher.class, arguments);
	}

	@Bean
	RestTemplate restTemplate() {
		return new RestTemplate();
	}
}
