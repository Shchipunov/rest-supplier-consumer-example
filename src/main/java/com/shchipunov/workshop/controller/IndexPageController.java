package com.shchipunov.workshop.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author Shchipunov Stanislav
 */
@Controller
public class IndexPageController {

	@RequestMapping(value = { "/" }, method = RequestMethod.GET)
	public String rootPage() {
		return "indexPage";
	}
}
