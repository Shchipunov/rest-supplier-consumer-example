package com.shchipunov.workshop.controller;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.shchipunov.workshop.domain.Contact;
import com.shchipunov.workshop.repository.ContactRepository;

/**
 * Supplies REST services. Remote server imitation.
 * 
 * @author Shchipunov Stanislav
 */
@RestController
@RequestMapping(value = { "/supplier/contacts" })
public class RemoteServerImitationController {

	private final ContactRepository contactRepository;

	@Autowired
	public RemoteServerImitationController(ContactRepository contactRepository) {
		this.contactRepository = Objects.requireNonNull(contactRepository);
	}

	/**
	 * Creates {@link Contact}
	 * @param contact
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST)
	public Contact createContact(@RequestBody Contact contact) {
		return contactRepository.save(contact);
	}

	/**
	 * Reads {@link Contact}
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = { "/{id}" }, method = RequestMethod.GET)
	public Contact readContact(@PathVariable Long id) {
		return contactRepository.findOne(id);
	}

	/**
	 * Reads all contacts
	 * 
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET)
	public List<Contact> readContacts() {
		return contactRepository.findAll();
	}

	/**
	 * Updates {@link Contact}
	 * 
	 * @param contact
	 */
	@RequestMapping(value = { "/{id}" }, method = RequestMethod.PUT)
	public void updateContact(@RequestBody Contact contact) {
		contactRepository.save(contact);
	}

	/**
	 * Deletes {@link Contact}
	 * 
	 * @param id
	 */
	@RequestMapping(value = { "/{id}" }, method = RequestMethod.DELETE)
	public void deleteContact(@PathVariable Long id) {
		contactRepository.delete(id);
	}
}
