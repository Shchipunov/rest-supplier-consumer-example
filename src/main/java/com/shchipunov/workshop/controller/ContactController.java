package com.shchipunov.workshop.controller;

import static org.springframework.http.HttpStatus.NOT_MODIFIED;
import static org.springframework.http.HttpStatus.NO_CONTENT;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.shchipunov.workshop.domain.Contact;
import com.shchipunov.workshop.exception.ContactNotModifiedException;

/**
 * Consumes REST services
 * 
 * @author Shchipunov Stanislav
 */
@RestController
@RequestMapping(value = { "/contacts" })
public class ContactController {

	private static final String REST_SUPPLIER_URL = "http://localhost:8080/supplier";

	private final RestTemplate restTemplate;

	@Autowired
	public ContactController(RestTemplate restTemplate) {
		this.restTemplate = Objects.requireNonNull(restTemplate);
	}

	/**
	 * Creates {@link Contact}
	 * 
	 * @param contact
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST)
	public Contact createContact(@RequestBody Contact contact) {
		return restTemplate.postForObject(REST_SUPPLIER_URL + "/contacts", contact, Contact.class);
	}

	/**
	 * Reads {@link Contact}
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = { "/{id}" }, method = RequestMethod.GET)
	public Contact readContact(@PathVariable Long id) {
		ResponseEntity<Contact> responseEntity = restTemplate.getForEntity(REST_SUPPLIER_URL + "/contacts/{id}",
				Contact.class, id);
		if (responseEntity.getStatusCode() == NOT_MODIFIED) {
			throw new ContactNotModifiedException();
		}
		return responseEntity.getBody();
	}

	/**
	 * Reads all contacts
	 * 
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET)
	public List<Contact> readContacts() {
		ResponseEntity<Contact[]> responseEntity = restTemplate.getForEntity(REST_SUPPLIER_URL + "/contacts",
				Contact[].class);
		return Arrays.asList(responseEntity.getBody());
	}

	/**
	 * Updates {@link Contact}
	 * 
	 * @param contact
	 */
	@RequestMapping(value = { "/{id}" }, method = RequestMethod.POST)
	@ResponseStatus(NO_CONTENT)
	public void updateContact(@RequestBody Contact contact) {
		restTemplate.put(REST_SUPPLIER_URL + "/contacts/{id}", contact, contact.getId());
	}

	/**
	 * Deletes {@link Contact}
	 * 
	 * @param id
	 */
	@RequestMapping(value = { "/{id}" }, method = RequestMethod.DELETE)
	@ResponseStatus(NO_CONTENT)
	public void deleteContact(@PathVariable Long id) {
		restTemplate.delete(REST_SUPPLIER_URL + "/contacts/{id}", id);
	}

	/**
	 * Handles {@link ContactNotModifiedException} exception
	 * 
	 * @param exception
	 * @return
	 */
	@ExceptionHandler(ContactNotModifiedException.class)
	@ResponseStatus(NOT_MODIFIED)
	public void handleContactNotModifiedException(ContactNotModifiedException exception) {

	}
}
