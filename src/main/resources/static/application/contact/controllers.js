angular.module("contactApplication")
	.controller("contactController", function($scope, $http, $resource, url) {
		$scope.$watchCollection("contacts", function() {
				$scope.totalContacts = $scope.contacts.length;
			});
		$scope.view = "table";
		$scope.currentContact = null;
		$scope.contactsResource = $resource(url + ":id", {
			id : "@id"
		});
		$scope.getContacts = function() {
			$scope.contacts = $scope.contactsResource.query();
		}
		$scope.deleteContact = function(contact) {
			contact.$delete().then(function() {
					$scope.contacts.splice($scope.contacts.indexOf(contact), 1);
					$scope.view = "table";
				});
		}
		$scope.createContact = function(contact) {
			new $scope.contactsResource(contact).$save().then(function(createdContact) {
					$scope.contacts.push(createdContact);
					$scope.view = "table";
				});
		}
		$scope.updateContact = function(contact) {
			contact.$save();
			$scope.view = "table";
		}
		$scope.editOrCreateContact = function(contact) {
			$scope.currentContact = contact ? contact : {};
			$scope.view = "editor";
		}
		$scope.saveEditedContact = function(contact) {
			if (angular.isDefined(contact.id)) {
				$scope.updateContact(contact);
			} else {
				$scope.createContact(contact);
			}
		}
		$scope.cancelEdit = function() {
			if ($scope.currentContact && $scope.currentContact.$get) {
				$scope.currentContact.$get();
			}
			$scope.currentContact = {};
			$scope.view = "table";
		}
		$scope.getContacts();
	});