package com.shchipunov.workshop.repository;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.shchipunov.workshop.domain.Contact;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class ContactRepositoryImplementationTests {

	private static final long EXISTING_CONTACT_ID = 1L;
	private static final String EXISTING_CONTACT_TYPE = "home phone";
	private static final String EXISTING_CONTACT_VALUE = "1234567";
	protected static final String NEW_CONTACT_TYPE = "mobile phone";
	protected static final String NEW_CONTACT_VALUE = "7654321";

	@Autowired
	ContactRepository contactRepository;

	@Test
	public void shouldFindById() {
		Contact contact = contactRepository.findOne(EXISTING_CONTACT_ID);

		assertThat(contact, notNullValue());
		assertThat(contact.getType(), equalTo(EXISTING_CONTACT_TYPE));
		assertThat(contact.getValue(), equalTo(EXISTING_CONTACT_VALUE));
	}

	@Test
	public void shouldFindByValue() {
		Contact contact = contactRepository.findByValue(EXISTING_CONTACT_VALUE);

		assertThat(contact, notNullValue());
		assertThat(contact.getId(), equalTo(EXISTING_CONTACT_ID));
		assertThat(contact.getType(), equalTo(EXISTING_CONTACT_TYPE));
	}

	@Test
	public void shouldFindAll() {
		List<Contact> contacts = contactRepository.findAll();

		assertThat(contacts.size(), equalTo(1));
	}

	@Test
	public void shouldSave() {
		Contact contact = new Contact();
		contact.setType(NEW_CONTACT_TYPE);
		contact.setValue(NEW_CONTACT_VALUE);

		contact = contactRepository.save(contact);

		assertThat(contact.getId(), equalTo(2L));
	}

	@Test
	public void shouldDelete() {
		contactRepository.delete(EXISTING_CONTACT_ID);

		assertThat(contactRepository.findAll().size(), equalTo(0));
	}
}
